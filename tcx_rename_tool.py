#!/usr/bin/env python

# -*- coding: utf-8 -*-

#  Polar ProTrainer (pdd + hrm + gpx) to Garmin (tcx) converter
#  Copyright (C) 2015-2019 Yanis Kurganov <yanis.kurganov@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys, os
from xml.dom.minidom import parse
from helpers import get_element, get_text, string_to_time, string_to_duration, time_to_string

def process(working_dir, hours, tcx_file_name):
    print("Processing '%s'..." % tcx_file_name)
    tcx = parse(os.path.join(working_dir, tcx_file_name))
    time = string_to_time(get_text(get_element(get_element(tcx.documentElement, "Activities"), "Activity"), "Id"), "%Y-%m-%dT%H:%M:%S.%fZ")
    time += string_to_duration("%s:00:00" % hours, "%H:%M:%S")
    os.rename(os.path.join(working_dir, tcx_file_name), os.path.join(working_dir, time_to_string(time, "%Y-%m-%d_%H-%M-%S") + ".tcx"))

def main(working_dir, hours):
    print("Polar ProTrainer to Garmin Converter")
    print("Copyright (C) Yanis Kurganov <yanis.kurganov@gmail.com>")
    print("Working directory: %s" % working_dir)
    for tcx_file_name in (file_name for file_name in os.listdir(working_dir) if os.path.isfile(os.path.join(working_dir, file_name)) and file_name.endswith(".tcx")):
        process(working_dir, hours, tcx_file_name)

main(sys.argv[1], sys.argv[2])
