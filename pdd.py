#!/usr/bin/env python

# -*- coding: utf-8 -*-

#  Polar ProTrainer (pdd + hrm + gpx) to Garmin (tcx) converter
#  Copyright (C) 2015-2019 Yanis Kurganov <yanis.kurganov@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

from collections import UserList
from logging import warning
from helpers import parse_config

class exercise_info:
    __MAGIC         = "101"
    __VERSION       = 0
    __SPORT         = 12
    __ENERGY        = 17
    __HRM_FILE_NAME = 152
    __GPX_FILE_NAME = 155
    __RR_FILE_NAME  = 156

    def __init__(self, section):
        if self.__MAGIC != section[self.__VERSION]:
            raise ValueError("Wrong signature")
        self.sport = section[self.__SPORT]
        self.energy = int(section[self.__ENERGY])
        if self.energy < 0:
            raise ValueError("Negative option 'Energy'")
        if self.energy > 10000:
            warning("Check the value of the option 'Energy'")
        self.hrm_file_name = section[self.__HRM_FILE_NAME]
        self.gpx_file_name = section[self.__GPX_FILE_NAME]
        self.rr_file_name = section[self.__RR_FILE_NAME]

class day_info(UserList):
    __MAGIC     = "100"
    __VERSION   = 0
    __EXERCISES = 7

    def __init__(self, path):
        config = parse_config(path)
        if "DayInfo" not in config:
            raise ValueError("Missing 'DayInfo' section")
        if self.__MAGIC != config["DayInfo"][self.__VERSION]:
            raise ValueError("Wrong signature")
        super().__init__()
        for i in range(1, 1 + int(config["DayInfo"][self.__EXERCISES])):
            if "ExerciseInfo%d" % i not in config:
                raise ValueError("Missing 'ExerciseInfo%d' section" % i)
            self.data.append(exercise_info(config["ExerciseInfo%d" % i]))
        if not self.data:
            warning("Empty Polar Diary Data")
